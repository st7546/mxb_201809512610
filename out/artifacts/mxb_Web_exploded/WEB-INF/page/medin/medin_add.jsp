<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <base href="<%=basePath%>">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport"
        content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <meta name="renderer" content="webkit">
  <link rel="stylesheet" type="text/css" href="<%=path %>/css/styles.css">
  <link rel="stylesheet" type="text/css" href="<%=path %>/css/admin.css">

  <script type="text/javascript" src="<%=path %>/js/jquery-1.4.4.min.js"></script>
  <script type="text/javascript" src="<%=path %>/js/admin.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      //重新绑定表单提交
      $("#add_btn").bind("click", function() {
        $('form').submit();
      });

    });
  </script>
</head>

<body>
<div class="panel admin-panel">
  <div class="panel-head" id="add"><strong><span class="icon-pencil-square-o"></span>增加医疗机构信息</strong></div>
  <div class="body-content">
    <form id="form-addrole" method="post" class="form-x" action="<%=path %>/medin/add">
      <input id="fid" name="fid" value="" type="hidden"/>
      <div class="form-group">
        <div class="label">
          <label>区域编号：</label>
        </div>
        <div class="field">
          <input type="text" class="input w50" value="" name="areaId" data-validate=" required:请输入区域编号" />
          <div class="tips"></div>
        </div>
      </div>
      <div class="form-group">
        <div class="label">
          <label>医疗机构编号：</label>
        </div>
        <div class="field">
          <input type="text" class="input w50" value="" name="medinId" data-validate=" required:请输入医疗机构编号" />
          <div class="tips"></div>
        </div>
      </div>
      <div class="form-group">
        <div class="label">
          <label>行政区域编号：</label>
        </div>
        <div class="field">
          <input type="text" class="input w50" value="" name="officeId" data-validate=" required:请输入行政区域编号" />
          <div class="tips"></div>
        </div>
      </div>
      <div class="form-group">
        <div class="label">
          <label>医疗机构名称：</label>
        </div>
        <div class="field">
          <input type="text" class="input w50" value="" name="medinName" data-validate=" required:请输入医疗机构名称" />
          <div class="tips"></div>
        </div>
      </div>
      <div class="form-group" >
        <div class="label">
          <label>隶属关系：</label>
        </div>
          <select class="input w50" id="lunch" name="bookTypeId">
            <option value="">请选择</option>
            <c:forEach items="${attachList}" var="data">
              <option value="${data.attachId}">${data.attachName}</option>
            </c:forEach>
          </select>
      </div>
      <div class="form-group">
        <div class="label">
          <label>医疗机构级别：</label>
        </div>
        <div class="field">
          <input type="text" class="input w50" value="" name="medinRank" data-validate=" required:请输入医疗机构级别" />
          <div class="tips"></div>
        </div>
      </div>
      <div class="form-group">
        <div class="label">
          <label>申报定点类型：</label>
        </div>
        <div class="field">
          <input type="text" class="input w50" value="" name="dfpType" data-validate=" required:请输入申报定点类型" />
          <div class="tips"></div>
        </div>
      </div>
      <div class="form-group">
        <div class="label">
          <label>批准定点类型：</label>
        </div>
        <div class="field">
          <input type="text" class="input w50" value="" name="afpType" data-validate=" required:请输入批准定点类型" />
          <div class="tips"></div>
        </div>
      </div>
      <div class="form-group">
        <div class="label">
          <label>机构所属经济类型：</label>
        </div>
        <div class="field">
          <input type="text" class="input w50" value="" name="economyType" data-validate=" required:请输入机构所属经济类型" />
          <div class="tips"></div>
        </div>
      </div>
      <div class="form-group">
        <div class="label">
          <label>组织机构编号：</label>
        </div>
        <div class="field">
          <input type="text" class="input w50" value="" name="organId" data-validate=" required:请输入组织机构编号" />
          <div class="tips"></div>
        </div>
      </div>
      <div class="form-group">
        <div class="label">
          <label>卫生机构(组织)：</label>
        </div>
        <div class="field">
          <input type="text" class="input w50" value="" name="organType" data-validate=" required:请输入卫生机构(组织)" />
          <div class="tips"></div>
        </div>
      </div>
      <div class="form-group">
        <div class="label">
          <label>主办单位：</label>
        </div>
        <div class="field">
          <input type="text" class="input w50" value="" name="organizers" data-validate=" required:请输入主办单位" />
          <div class="tips"></div>
        </div>
      </div>
      <div class="form-group">
        <div class="label">
          <label>成立时间：</label>
        </div>
        <div class="field">
          <input type="text" class="input w50" value="" name="builtTime" data-validate=" required:请输入成立时间" />
          <div class="tips"></div>
        </div>
      </div>
      <div class="form-group">
        <div class="label">
          <label>法定负责人：</label>
        </div>
        <div class="field">
          <input type="text" class="input w50" value="" name="prin" data-validate=" required:请输入法定负责人" />
          <div class="tips"></div>
        </div>
      </div>
      <div class="form-group">
        <div class="label">
          <label>注册基金：</label>
        </div>
        <div class="field">
          <input type="text" class="input w50" value="" name="reca" data-validate=" required:请输入法定负责人" />
          <div class="tips"></div>
        </div>
      </div>

      <div class="form-group">
        <div class="label">
          <label></label>
        </div>
        <div class="field">
          <button id="add_btn" class="button bg-main icon-check-square-o" type="button"> 提交</button>
        </div>
      </div>
    </form>
  </div>
</div>


</body>
</html>
