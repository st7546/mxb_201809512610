package com.gxuwz.mxb.lph.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class Medin implements Serializable {
    private Integer id;

    private String areaId;

    private String medinId;

    private String officeId;

    private String medinName;

    private String attached;

    private String medinRank;

    private String dfpType;

    private String afpType;

    private String economyType;

    private String organId;

    private String organType;

    private String organizers;

    private String builtTime;

    private String prin;

    private String reca;

    //private String attachName;

    private Date createTime;

    private Date updateTime;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId == null ? null : areaId.trim();
    }

    public String getMedinId() {
        return medinId;
    }

    public void setMedinId(String medinId) {
        this.medinId = medinId == null ? null : medinId.trim();
    }

    public String getOfficeId() {
        return officeId;
    }

    public void setOfficeId(String officeId) {
        this.officeId = officeId == null ? null : officeId.trim();
    }

    /*public String getAttachName() {
        return attachName;
    }*/

    /*public void setAttachName(String attachName) {
        this.attachName = attachName == null ? null : attachName.trim();
    }*/

    public String getMedinName() {
        return medinName;
    }

    public void setMedinName(String medinName) {
        this.medinName = medinName == null ? null : medinName.trim();
    }

    public String getAttached() {
        return attached;
    }

    public void setAttached(String attached) {
        this.attached = attached == null ? null : attached.trim();
    }

    public String getMedinRank() {
        return medinRank;
    }

    public void setMedinRank(String medinRank) {
        this.medinRank = medinRank == null ? null : medinRank.trim();
    }

    public String getDfpType() {
        return dfpType;
    }

    public void setDfpType(String dfpType) {
        this.dfpType = dfpType == null ? null : dfpType.trim();
    }

    public String getAfpType() {
        return afpType;
    }

    public void setAfpType(String afpType) {
        this.afpType = afpType == null ? null : afpType.trim();
    }

    public String getEconomyType() {
        return economyType;
    }

    public void setEconomyType(String economyType) {
        this.economyType = economyType == null ? null : economyType.trim();
    }

    public String getOrganId() {
        return organId;
    }

    public void setOrganId(String organId) {
        this.organId = organId == null ? null : organId.trim();
    }

    public String getOrganType() {
        return organType;
    }

    public void setOrganType(String organType) {
        this.organType = organType == null ? null : organType.trim();
    }

    public String getOrganizers() {
        return organizers;
    }

    public void setOrganizers(String organizers) {
        this.organizers = organizers == null ? null : organizers.trim();
    }

    public String getBuiltTime() {
        return builtTime;
    }

    public void setBuiltTime(String builtTime) {
        this.builtTime = builtTime == null ? null : builtTime.trim();
    }

    public String getPrin() {
        return prin;
    }

    public void setPrin(String prin) {
        this.prin = prin == null ? null : prin.trim();
    }

    public String getReca() {
        return reca;
    }

    public void setReca(String reca) {
        this.reca = reca == null ? null : reca.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", areaId=").append(areaId);
        sb.append(", medinId=").append(medinId);
        sb.append(", officeId=").append(officeId);
        sb.append(", medinName=").append(medinName);
        //sb.append(", attachName=").append(attachName);
        sb.append(", attached=").append(attached);
        sb.append(", medinRank=").append(medinRank);
        sb.append(", dfpType=").append(dfpType);
        sb.append(", afpType=").append(afpType);
        sb.append(", economyType=").append(economyType);
        sb.append(", organId=").append(organId);
        sb.append(", organType=").append(organType);
        sb.append(", organizers=").append(organizers);
        sb.append(", builtTime=").append(builtTime);
        sb.append(", prin=").append(prin);
        sb.append(", reca=").append(reca);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}