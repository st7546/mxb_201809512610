package com.gxuwz.mxb.lph.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class Policy implements Serializable {
    private Integer id;

    private String mxbpolicyId;

    private String poYear;

    private String poMax;

    private String mxbRatio;

    private Date createTime;

    private Date updateTime;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMxbpolicyId() {
        return mxbpolicyId;
    }

    public void setMxbpolicyId(String mxbpolicyId) {
        this.poYear = poYear == null ? null : poYear.trim();
    }

    public String getPoYear() {
        return poYear;
    }

    public void setPoYear(String poYear) {
        this.poYear = poYear == null ? null : poYear.trim();
    }

    public String getPoMax() {
        return poMax;
    }

    public void setPoMax(String poMax) {
        this.poMax = poMax == null ? null : poMax.trim();
    }

    public String getMxbRatio() {
        return mxbRatio;
    }

    public void setMxbRatio(String mxbRatio) {
        this.mxbRatio = mxbRatio == null ? null : mxbRatio.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", mxbpolicyId=").append(mxbpolicyId);
        sb.append(", poYear=").append(poYear);
        sb.append(", poMax=").append(poMax);
        sb.append(", mxbRatio=").append(mxbRatio);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}