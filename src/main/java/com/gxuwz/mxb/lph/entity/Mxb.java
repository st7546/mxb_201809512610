package com.gxuwz.mxb.lph.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class Mxb implements Serializable {
    private Integer id;

    private String mxbId;

    private String mxbPj;

    private String mxbName;

    private Date createTime;

    private Date updateTime;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMxbId() {
        return mxbId;
    }

    public void setMxbId(String mxbId) {
        this.mxbId = mxbId == null ? null : mxbId.trim();
    }

    public String getMxbPj() {
        return mxbPj;
    }

    public void setMxbPj(String mxbPj) {
        this.mxbPj = mxbPj == null ? null : mxbPj.trim();
    }

    public String getMxbName() {
        return mxbName;
    }

    public void setMxbName(String mxbName) {
        this.mxbName = mxbName == null ? null : mxbName.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", mxbId=").append(mxbId);
        sb.append(", mxbPj=").append(mxbPj);
        sb.append(", mxbName=").append(mxbName);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}