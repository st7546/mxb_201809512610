package com.gxuwz.mxb.lph.dao;

import com.gxuwz.mxb.lph.entity.RuralCmsOffice;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RuralCmsOfficeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(RuralCmsOffice record);

    RuralCmsOffice selectByPrimaryKey(Integer id);

    RuralCmsOffice findOfficeByOfficeId(Integer officeId);

    List<RuralCmsOffice> selectAll();

    List<RuralCmsOffice> getOfficeList(@Param("officeName") String officeName);

    int updateByPrimaryKey(RuralCmsOffice record);
}