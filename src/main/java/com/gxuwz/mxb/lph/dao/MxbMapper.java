package com.gxuwz.mxb.lph.dao;

import com.gxuwz.mxb.lph.entity.Mxb;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MxbMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Mxb record);

    Mxb selectByPrimaryKey(Integer id);

    Mxb findMxbByMxbId(Integer mxbId);

    List<Mxb> selectAll();

    List<Mxb> getMxbList(@Param("mxbName") String mxbName);

    int updateByPrimaryKey(Mxb record);
}