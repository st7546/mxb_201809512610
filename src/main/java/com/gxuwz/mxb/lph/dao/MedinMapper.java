package com.gxuwz.mxb.lph.dao;

import com.gxuwz.mxb.lph.entity.Medin;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MedinMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Medin record);

    Medin selectByPrimaryKey(Integer id);

    Medin findMedinByMedinId(Integer medinId);

    List<Medin> selectAll();

    List<Medin> getMedinList(@Param("medinName") String medinName);

    //List<Medin> getAttachList(@Param("attachName") String attachName);

    int updateByPrimaryKey(Medin record);
}