package com.gxuwz.mxb.lph.dao;

import com.gxuwz.mxb.lph.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    User selectByPrimaryKey(Integer id);

    User findUserByUserId(String userId);

    List<User> selectAll();

    List<User> getUserList(@Param("userName") String userName);

    int updateByPrimaryKey(User record);
}