package com.gxuwz.mxb.lph.dao;

import com.gxuwz.mxb.lph.entity.Policy;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PolicyMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Policy record);

    Policy selectByPrimaryKey(Integer id);

    Policy findPolicyByMxbpolicyId(String mxbpolicyId);

    List<Policy> selectAll();

    List<Policy> getPolicyList(@Param("mxbpolicyId") String mxbpolicyId);

    int updateByPrimaryKey(Policy record);
}