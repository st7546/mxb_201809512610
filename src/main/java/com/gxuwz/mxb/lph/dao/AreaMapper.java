package com.gxuwz.mxb.lph.dao;

import com.gxuwz.mxb.lph.entity.Area;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AreaMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Area record);

    Area selectByPrimaryKey(Integer id);

    Area findAreaByAreaId(Integer areaId);

    List<Area> selectAll();

    List<Area>  getAreaList(@Param("areaName") String areaName);

    int updateByPrimaryKey(Area record);

    /**
     * 根据areaId获取所有子区域信息列表
     * @return
     */
    List<Area> getAreaChilderList(@Param("areaId") String areaId);

}