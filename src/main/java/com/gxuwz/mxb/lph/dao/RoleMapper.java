package com.gxuwz.mxb.lph.dao;

import com.gxuwz.mxb.lph.entity.Role;
import com.gxuwz.mxb.lph.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RoleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Role record);

    Role selectByPrimaryKey(Integer id);

    Role findRoleByRoleId(String roleId);

    List<Role> selectAll();

    List<Role> getRoleList(@Param("roleName") String roleName);

    int updateByPrimaryKey(Role record);
}