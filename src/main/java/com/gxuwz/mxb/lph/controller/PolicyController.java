package com.gxuwz.mxb.lph.controller;

import com.github.pagehelper.PageInfo;
import com.gxuwz.mxb.lph.entity.Policy;
import com.gxuwz.mxb.lph.service.PolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/policy")
public class PolicyController {
    @Autowired
    private PolicyService policyService;
    /**
     * 获取信息列表
     */
    @RequestMapping("/getPolicyList")
    public String getPolicyList(Integer currentPage, Integer limit, HttpServletRequest request, Model model){
        String mxbpolicyId = request.getParameter("context");
        currentPage = currentPage != null? currentPage : 1;
        limit = limit != null ? limit : 10;

        PageInfo<Policy> pageInfo = policyService.getPolicyList(mxbpolicyId, currentPage, limit);
        model.addAttribute("pageInfo", pageInfo);
        model.addAttribute("context", mxbpolicyId);
        return "/policy/policy_list";
    }
    /**
     * 打开添加页面
     */
    @RequestMapping(    "/openAdd")
    public String openAdd(Model model) {
        return "/policy/policy_add";
    }

    /**
     * 添加用户信息
     */
    @RequestMapping("/add")
    public String add(Policy policy, HttpServletRequest request, Model model) {
        policyService.addPolicy(policy);
        return getPolicyList(1, 10, request, model);
    }

    /**
     * 打开编辑页面
     */
    @RequestMapping("/openEdit")
    public String openEdit(Integer id, Model model) {
        Policy policy = policyService.selectByPrimaryKey(id);
        model.addAttribute("policy",policy);
        return "/policy/policy_edit";
    }

    /**
     * 更新
     */
    @RequestMapping("/edit")
    public String edit(Policy policy, HttpServletRequest request, Model model) {
        policyService.edit(policy);
        return getPolicyList(1, 10, request, model);
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public String delete(Integer id, HttpServletRequest request, Model model) {
        policyService.delete(id);
        return getPolicyList(1, 10, request, model);
    }
}
