package com.gxuwz.mxb.lph.controller;

import com.github.pagehelper.PageInfo;
import com.gxuwz.mxb.lph.entity.Area;
import com.gxuwz.mxb.lph.service.AreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/area")
public class AreaController {
    @Autowired
    private AreaService areaService;
    /**
     * 获取角色信息列表
     */
    @RequestMapping("/getAreaList")
    public String getAreaList(Integer currentPage, Integer limit, HttpServletRequest request, Model model){
        String areaName = request.getParameter("context");
        currentPage = currentPage != null? currentPage : 1;
        limit = limit != null ? limit : 10;

        PageInfo<Area> pageInfo = areaService.getAreaList(areaName, currentPage, limit);
        model.addAttribute("pageInfo", pageInfo);
        model.addAttribute("context", areaName);
        return "/area/area_list";
    }

    /**
     * 打开添加页面
     */
    @RequestMapping("/openAdd")
    public String openAdd(Model model) {
        return "/area/area_add";
    }

    /**
     * 添加用户信息
     */
    @RequestMapping("/add")
    public String add(Area area, HttpServletRequest request, Model model) {
        areaService.addArea(area);
        return getAreaList(1, 10, request, model);
    }
    /**
     * 打开编辑页面
     */
    @RequestMapping("/openEdit")
    public String openEdit(Integer id, Model model) {
        Area area = areaService.selectByPrimaryKey(id);
        model.addAttribute("area",area);
        return "/area/area_edit";
    }

    /**
     * 更新
     */
    @RequestMapping("/edit")
    public String edit(Area area, HttpServletRequest request, Model model) {
        areaService.edit(area);
        return getAreaList(1, 10, request, model);
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public String delete(Integer id, HttpServletRequest request, Model model) {
        areaService.delete(id);
        return getAreaList(1, 10, request, model);
    }


    @RequestMapping("/getAreaTreeList")
    @ResponseBody
    /*
    public Object getAreaTreeList() {
        Map<String, Object> map = new HashMap<String, Object>();
        List<Area> areaList = areaService.getAreaTree("");
        map.put("areaList", areaList);
        // 使用json函数手动封装
        String json  =  new JSONObject(map).toString();
        return json;
    }*/
    public Object getAreaTreeList() {
        Map<String, Object> map = new HashMap<String, Object>();
        List<Area> areaList = areaService.getAreaTree("");
        System.out.println("arealist:"+areaList);
        map.put("areaList", areaList);
        return map;
    }

//    /**
//     * 传Arealist给家庭档案
//     * @param areaName
//     * @return
//     */
//    @RequestMapping(value="/findAreaList")
//    @ResponseBody
//    public Map<String, Object> findAreaList(String areaName){
//        System.out.println("areaName:"+areaName);
//        List<Area> areaList= areaService.getAreaTree(areaName);
//        Map<String, Object> map = new HashMap<String, Object>();
//        map.put("areaList", areaList);
//        return map;
//    }

}
