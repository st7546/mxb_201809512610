package com.gxuwz.mxb.lph.controller;

import com.github.pagehelper.PageInfo;
import com.gxuwz.mxb.lph.entity.Area;
import com.gxuwz.mxb.lph.entity.Medin;
import com.gxuwz.mxb.lph.service.MedinAttService;
import com.gxuwz.mxb.lph.service.MedinService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/medin")
public class MedinController {
    @Autowired
    private MedinService medinService;

    @Autowired
    private MedinAttService medinAttService;

    /**
     * 获取用户信息列表
     */
    @RequestMapping("/getMedinList")
    public String getMedinList(Integer currentPage, Integer limit, HttpServletRequest request, Model model){
        String medinName = request.getParameter("context");
        currentPage = currentPage != null? currentPage : 1;
        limit = limit != null ? limit : 10;

        PageInfo<Medin> pageInfo = medinService.getMedinList(medinName, currentPage, limit);
        model.addAttribute("pageInfo", pageInfo);
        model.addAttribute("context", medinName);
        return "/medin/medin_list";
    }
    /**
     * 打开添加页面
     */
    @RequestMapping("/openAdd")
    public String openAdd(Model model) {
        model.addAttribute("attachList",medinAttService.getAttach(""));
        return "/medin/medin_add";
    }

    /**
     * 添加用户信息
     */
    @RequestMapping("/add")
    public String add(Medin medin, HttpServletRequest request, Model model) {
        medinService.addMedin(medin);
        return getMedinList(1, 10, request, model);
    }

    /**
     * 打开编辑页面
     */
    @RequestMapping("/openEdit")
    public String openEdit(Integer id, Model model) {
        Medin medin = medinService.selectByPrimaryKey(id);
        model.addAttribute("medin",medin);
        return "/medin/medin_edit";
    }

    /**
     * 更新
     */
    @RequestMapping("/edit")
    public String edit(Medin medin, HttpServletRequest request, Model model) {
        medinService.edit(medin);
        return getMedinList(1, 10, request, model);
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public String delete(Integer id, HttpServletRequest request, Model model) {
        medinService.delete(id);
        return getMedinList(1, 10, request, model);
    }
    /**
     *

    @RequestMapping("/getMedinAttachList")
    @ResponseBody
    public Object getMadinAttachList() {
        ModelAndView ma = new ModelAndView();
        Map<String, Object> map = new HashMap<String, Object>();
        //查询隶属类型
        List<Medin> attachList = medinService.getAttach("");
        map.put("attachList", attachList);
        ma.addAllObjects(map);//返回数据
        ma.setViewName("/medin/medin_add");//跳转链接
        return map;
    }*/
}
