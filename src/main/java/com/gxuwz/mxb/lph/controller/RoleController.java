package com.gxuwz.mxb.lph.controller;

import com.github.pagehelper.PageInfo;
import com.gxuwz.mxb.lph.entity.Role;
import com.gxuwz.mxb.lph.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/role")
public class RoleController {
    @Autowired
    private RoleService roleService;

    /**
     * 获取角色信息列表
     */
    @RequestMapping("/getRoleList")
    public String getRoleList(Integer currentPage, Integer limit, HttpServletRequest request, Model model){
        String roleName = request.getParameter("context");
        currentPage = currentPage != null? currentPage : 1;
        limit = limit != null ? limit : 10;

        PageInfo<Role> pageInfo = roleService.getRoleList(roleName, currentPage, limit);
        model.addAttribute("pageInfo", pageInfo);
        model.addAttribute("context", roleName);
        return "/role/role_list";
    }
    /**
     * 打开添加页面
     */
    @RequestMapping("/openAdd")
    public String openAdd(Model model) {
        return "/role/role_add";
    }

    /**
     * 添加用户信息
     */
    @RequestMapping("/add")
    public String add(Role role, HttpServletRequest request, Model model) {
        roleService.addRole(role);
        return getRoleList(1, 10, request, model);
    }

    /**
     * 打开编辑页面
     */
    @RequestMapping("/openEdit")
    public String openEdit(Integer id, Model model) {
        Role role = roleService.selectByPrimaryKey(id);
        model.addAttribute("role",role);
        return "/role/role_edit";
    }

    /**
     * 更新
     */
    @RequestMapping("/edit")
    public String edit(Role role, HttpServletRequest request, Model model) {
        roleService.edit(role);
        return getRoleList(1, 10, request, model);
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public String delete(Integer id, HttpServletRequest request, Model model) {
        roleService.delete(id);
        return getRoleList(1, 10, request, model);
    }
}
