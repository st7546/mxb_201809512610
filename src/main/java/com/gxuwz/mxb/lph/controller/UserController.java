package com.gxuwz.mxb.lph.controller;

import com.github.pagehelper.PageInfo;
import com.gxuwz.mxb.lph.entity.User;
import com.gxuwz.mxb.lph.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    /**
     * 获取用户信息列表
     */
    @RequestMapping("/getUserList")
    public String   getUserList(Integer currentPage, Integer limit, HttpServletRequest request, Model model){
        String userName = request.getParameter("context");
        currentPage = currentPage != null? currentPage : 1;
        limit = limit != null ? limit : 10;

        PageInfo<User> pageInfo = userService.getUserList(userName, currentPage, limit);
        model.addAttribute("pageInfo", pageInfo);
        model.addAttribute("context", userName);
        return "/user/user_list";
    }
    /**
     * 打开添加页面
     */
    @RequestMapping("/openAdd")
    public String openAdd(Model model) {
        return "/user/user_add";
    }

    /**
     * 添加用户信息
     */
    @RequestMapping("/add")
    public String add(User user, HttpServletRequest request, Model model) {
        userService.addUser(user);
        return getUserList(1, 10, request, model);
    }

    /**
     * 打开编辑页面
     */
    @RequestMapping("/openEdit")
    public String openEdit(Integer id, Model model) {
        User user = userService.selectByPrimaryKey(id);
        model.addAttribute("user",user);
        return "/user/user_edit";
    }

    /**
     * 更新
     */
    @RequestMapping("/edit")
    public String edit(User user, HttpServletRequest request, Model model) {
        userService.edit(user);
        return getUserList(1, 10, request, model);
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public String delete(Integer id, HttpServletRequest request, Model model) {
        userService.delete(id);
        return getUserList(1, 10, request, model);
    }
}
