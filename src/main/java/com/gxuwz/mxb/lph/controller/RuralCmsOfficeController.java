package com.gxuwz.mxb.lph.controller;

import com.github.pagehelper.PageInfo;
import com.gxuwz.mxb.lph.entity.RuralCmsOffice;
import com.gxuwz.mxb.lph.service.RuralCmsOfficeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/office")
public class RuralCmsOfficeController {
    @Autowired
    private RuralCmsOfficeService ruralCmsOfficeService;
    /**
     * 获取用户信息列表
     */
    @RequestMapping("/getOfficeList")
    public String getOfficeList(Integer currentPage, Integer limit, HttpServletRequest request, Model model){
        String officeName = request.getParameter("context");
        currentPage = currentPage != null? currentPage : 1;
        limit = limit != null ? limit : 10;

        PageInfo<RuralCmsOffice> pageInfo = ruralCmsOfficeService.getOfficeList(officeName, currentPage, limit);
        model.addAttribute("pageInfo", pageInfo);
        model.addAttribute("context", officeName);
        return "/office/office_list";
    }
    /**
     * 打开添加页面
     */
    @RequestMapping("/openAdd")
    public String openAdd(Model model) {
        return "/office/office_add";
    }

    /**
     * 添加用户信息
     */
    @RequestMapping("/add")
    public String add(RuralCmsOffice ruralCmsOffice, HttpServletRequest request, Model model) {
        ruralCmsOfficeService.addOffice(ruralCmsOffice);
        return getOfficeList(1, 10, request, model);
    }

    /**
     * 打开编辑页面
     */
    @RequestMapping("/openEdit")
    public String openEdit(Integer id, Model model) {
        RuralCmsOffice ruralCmsOffice = ruralCmsOfficeService.selectByPrimaryKey(id);
        model.addAttribute("ruralCmsOffice",ruralCmsOffice);
        return "/office/office_edit";
    }

    /**
     * 更新
     */
    @RequestMapping("/edit")
    public String edit(RuralCmsOffice ruralCmsOffice, HttpServletRequest request, Model model) {
        ruralCmsOfficeService.edit(ruralCmsOffice);
        return getOfficeList(1, 10, request, model);
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public String delete(Integer id, HttpServletRequest request, Model model) {
        ruralCmsOfficeService.delete(id);
        return getOfficeList(1, 10, request, model);
    }
}
