package com.gxuwz.mxb.lph.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.mxb.lph.dao.MxbMapper;
import com.gxuwz.mxb.lph.entity.Mxb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;

@Service
@Transactional
public class MxbService {
    @Autowired
    private MxbMapper mxbMapper;
    /**
     * 根据角色ID查询角色信息
     */
    public Mxb findMxbByMxbId(Integer mxbId){
        return mxbMapper.findMxbByMxbId(mxbId);
    }
    /**
     * 根据id 查询角色信息
     */
    public Mxb selectByPrimaryKey(Integer id){
        return mxbMapper.selectByPrimaryKey(id);
    }
    /**
     * 查找列表
     */
    public PageInfo<Mxb> getMxbList(String mxbName, Integer currentPage, Integer limit){
        //利用PageHelper分页查询 注意：这个一定要放查询语句的前一行,否则无法进行分页,因为它对紧随其后第一个sql语句有效
        PageHelper.startPage(currentPage,limit);
        List<Mxb> mxbList = mxbMapper.getMxbList(mxbName);
        PageInfo<Mxb> pageInfo = new PageInfo<Mxb>(mxbList);
        return pageInfo;
    }
    //MxbProofatt调用
    public List<Mxb> getMxbsList(String mxbName){
        //利用PageHelper分页查询 注意：这个一定要放查询语句的前一行,否则无法进行分页,因为它对紧随其后第一个sql语句有效
        List<Mxb> mxbList = mxbMapper.getMxbList(mxbName);
        return mxbList;
    }
    /**
     * 添加
     */
    public void addMxb(Mxb mxb){
        mxb.setCreateTime(new Timestamp(System.currentTimeMillis()));
        mxb.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        mxbMapper.insert(mxb);
    }

    /**
     *修改
     */
    public void edit(Mxb mxb){
        mxb.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        mxbMapper.updateByPrimaryKey(mxb);
    }

    /**
     * 删除
     *
     */
    public void delete(Integer id){
        mxbMapper.deleteByPrimaryKey(id);
    }
}
