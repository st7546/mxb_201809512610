package com.gxuwz.mxb.lph.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.mxb.lph.dao.RuralCmsOfficeMapper;
import com.gxuwz.mxb.lph.entity.RuralCmsOffice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;

@Service
@Transactional
public class RuralCmsOfficeService {
    @Autowired
    private RuralCmsOfficeMapper ruralCmsOfficeMapper;
    /**
     * 根据合办处ID查询角色信息
     */
    public RuralCmsOffice findOfficeByOfficeId(Integer officeId){
        return ruralCmsOfficeMapper.findOfficeByOfficeId(officeId);
    }
    /**
     * 根据id 查询角色信息
     */
    public RuralCmsOffice selectByPrimaryKey(Integer id){
        return ruralCmsOfficeMapper.selectByPrimaryKey(id);
    }
    /**
     * 查找合办列表
     */
    public PageInfo<RuralCmsOffice> getOfficeList(String officeName, Integer currentPage, Integer limit){
        //利用PageHelper分页查询 注意：这个一定要放查询语句的前一行,否则无法进行分页,因为它对紧随其后第一个sql语句有效
        PageHelper.startPage(currentPage,limit);
        List<RuralCmsOffice> officeList = ruralCmsOfficeMapper.getOfficeList(officeName);
        PageInfo<RuralCmsOffice> pageInfo = new PageInfo<RuralCmsOffice>(officeList);
        return pageInfo;
    }
    /**
     * 添加
     */
    public void addOffice(RuralCmsOffice ruralCmsOffice){
        ruralCmsOffice.setCreateTime(new Timestamp(System.currentTimeMillis()));
        ruralCmsOffice.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        ruralCmsOfficeMapper.insert(ruralCmsOffice);
    }

    /**
     *修改
     */
    public void edit(RuralCmsOffice ruralCmsOffice){
        ruralCmsOffice.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        ruralCmsOfficeMapper.updateByPrimaryKey(ruralCmsOffice);
    }

    /**
     * 删除
     *
     */
    public void delete(Integer id){
        ruralCmsOfficeMapper.deleteByPrimaryKey(id);
    }
}
