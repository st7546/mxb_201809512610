package com.gxuwz.mxb.lph.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.mxb.lph.dao.HouseholeMemberMapper;
import com.gxuwz.mxb.lph.dao.HouseholefileMapper;
import com.gxuwz.mxb.lph.dao.MxbProofattMapper;
import com.gxuwz.mxb.lph.entity.MxbProofatt;
import com.gxuwz.mxb.lph.entity.User;
import jdk.nashorn.internal.ir.IdentNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class MxbProofattService {
    @Autowired
    private HouseholefileMapper householefileMapper;
    @Autowired
    private HouseholeMemberMapper householeMemberMapper;
    @Autowired
    private MxbProofattMapper mxbProofattMapper;

    /**
     * 根据ID查找信息
     */
    public MxbProofatt selectByPrimaryKey(Integer id){
        return mxbProofattMapper.selectByPrimaryKey(id);
    }

    /**
     * 根据姓名查找户主信息列表
     */
    public PageInfo<MxbProofatt> getProofattList(String memberName, Integer currentPage, Integer limit){
        //利用PageHelper分页查询 注意：这个一定要放查询语句的前一行,否则无法进行分页,因为它对紧随其后第一个sql语句有效
        PageHelper.startPage(currentPage,limit);
        List<MxbProofatt> proofattList = mxbProofattMapper.getProofattList(memberName);

        PageInfo<MxbProofatt> pageInfo = new PageInfo<MxbProofatt>(proofattList);
        return pageInfo;
    }

    /**
     * 获取慢性病状态
     * @param memberName
     * @return
     */
    public boolean getProofattState(String memberName){
        String payState = null;

        List<MxbProofatt> mxbProofattList = mxbProofattMapper.getProofattList(memberName);
        for(int i=0;i<mxbProofattList.size();i++){
            payState = mxbProofattList.get(i).getProofattState();
        }
        return payState != null && payState.equals("是");
    }

    /**
     * 获取用户类型
     * @param memberName
     * @return
     */
    public boolean getMemberType(String memberName){
        String memberType = null;

        List<MxbProofatt> mxbProofattList = mxbProofattMapper.getProofattList(memberName);
        for(int i = 0;i<mxbProofattList.size();i++){
            memberType = mxbProofattList.get(i).getMemberType();
        }
        boolean a = memberType != null && memberType.equals("是");
        System.out.println("++++++:"+a);
        return a;
    }

    /**
     * 查找参合家庭未缴费列表
     */
    public PageInfo<MxbProofatt> getPayMemberList(String householdId, Integer currentPage, Integer limit){
        String payState = null;
        String type = null;
//        List <String> householdId1 = new ArrayList<String>();
        //利用PageHelper分页查询 注意：这个一定要放查询语句的前一行,否则无法进行分页,因为它对紧随其后第一个sql语句有效
        PageHelper.startPage(currentPage,limit);
        List<MxbProofatt> mxbProofattList = mxbProofattMapper.getMemberListByHouseholdId(householdId);

        List<MxbProofatt> payEnrolment = new ArrayList<MxbProofatt>();

        for(int i=0;i<mxbProofattList.size();i++){
            payEnrolment = mxbProofattMapper.getProofattState(mxbProofattList.get(i).getHouseholdId(),mxbProofattList.get(i).getProofattState());
        }

        PageInfo<MxbProofatt> pageInfo = new PageInfo<MxbProofatt>(payEnrolment);
        return pageInfo;
    }

    /**
     * 根据户内编号查找列表
     * @param idNum
     * @return
     */
    public List<MxbProofatt> getMemberListByIdNum(String idNum) {
        System.out.println("getMemberListByHouseinId:"+idNum);
        return mxbProofattMapper.getMemberListByIdNum(idNum);
    }

    /**
     * 添加
     */
    public void addProofatt(MxbProofatt mxbProofatt){
        mxbProofatt.setCreateTime(new Timestamp(System.currentTimeMillis()));
        mxbProofatt.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        mxbProofattMapper.insert(mxbProofatt);
    }
    /**
     *修改
     */
    public void edit(MxbProofatt mxbProofatt){
        mxbProofatt.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        mxbProofattMapper.updateByPrimaryKey(mxbProofatt);
    }
    /**
     * 删除
     *
     */
    public void delete(Integer id){
        mxbProofattMapper.deleteByPrimaryKey(id);
    }
}
