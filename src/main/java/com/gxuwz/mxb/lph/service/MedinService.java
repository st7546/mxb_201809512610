package com.gxuwz.mxb.lph.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.mxb.lph.dao.MedinMapper;
import com.gxuwz.mxb.lph.entity.Area;
import com.gxuwz.mxb.lph.entity.Medin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;

@Service
@Transactional
public class MedinService {
    @Autowired
    private MedinMapper medinMapper;
    /**
     * 根据合办处ID查询角色信息
     */
    public Medin findOfficeByOfficeId(Integer mdeinId){
        return medinMapper.findMedinByMedinId(mdeinId);
    }
    /**
     * 根据id 查询角色信息
     */
    public Medin selectByPrimaryKey(Integer id){
        return medinMapper.selectByPrimaryKey(id);
    }
    /**
     * 查找合办列表
     */
    public PageInfo<Medin> getMedinList(String medinName, Integer currentPage, Integer limit){
        //利用PageHelper分页查询 注意：这个一定要放查询语句的前一行,否则无法进行分页,因为它对紧随其后第一个sql语句有效
        PageHelper.startPage(currentPage,limit);
        List<Medin> medinList = medinMapper.getMedinList(medinName);
        PageInfo<Medin> pageInfo = new PageInfo<Medin>(medinList);
        return pageInfo;
    }
    /**
     * MxbProofatt调用
     */
    public List<Medin> getMedinsList(String medinName){
        List<Medin> medinList = medinMapper.getMedinList(medinName);
        return medinList;
    }
    /**
     * 添加
     */
    public void addMedin(Medin medin){
        medin.setCreateTime(new Timestamp(System.currentTimeMillis()));
        medin.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        medinMapper.insert(medin);
    }

    /**
     *修改
     */
    public void edit(Medin medin){
        medin.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        medinMapper.updateByPrimaryKey(medin);
    }

    /**
     * 删除
     *
     */
    public void delete(Integer id){
        medinMapper.deleteByPrimaryKey(id);
    }

    /*public List<Medin> getAttach(String attachName) {
        return medinMapper.getAttachList(attachName);
    }*/
}
