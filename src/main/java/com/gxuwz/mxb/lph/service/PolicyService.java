package com.gxuwz.mxb.lph.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.mxb.lph.dao.PolicyMapper;
import com.gxuwz.mxb.lph.entity.Policy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;

@Service
@Transactional
public class PolicyService {
    @Autowired
    private PolicyMapper policyMapper;
    /**
     * 根据角色ID查询角色信息
     */
    public Policy findPolicyByMxbpolicyId(String mxbpolicyId){
        return policyMapper.findPolicyByMxbpolicyId(mxbpolicyId);
    }
    /**
     * 根据id 查询角色信息
     */
    public Policy selectByPrimaryKey(Integer id){
        return policyMapper.selectByPrimaryKey(id);
    }
    /**
     * 查找角色列表
     */
    public PageInfo<Policy> getPolicyList(String mxbpolicyId, Integer currentPage, Integer limit){
        //利用PageHelper分页查询 注意：这个一定要放查询语句的前一行,否则无法进行分页,因为它对紧随其后第一个sql语句有效
        PageHelper.startPage(currentPage,limit);
        List<Policy> policyList = policyMapper.getPolicyList(mxbpolicyId);
        PageInfo<Policy> pageInfo = new PageInfo<Policy>(policyList);
        return pageInfo;
    }
    /**
     * 添加
     */
    public void addPolicy(Policy policy){
        policy.setCreateTime(new Timestamp(System.currentTimeMillis()));
        policy.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        policyMapper.insert(policy);
    }

    /**
     *修改
     */
    public void edit(Policy policy){
        policy.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        policyMapper.updateByPrimaryKey(policy);
    }

    /**
     * 删除
     *
     */
    public void delete(Integer id){
        policyMapper.deleteByPrimaryKey(id);
    }
}
