package com.gxuwz.mxb.lph.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.mxb.lph.dao.AreaMapper;
import com.gxuwz.mxb.lph.entity.Area;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class AreaService {
    @Autowired
    private AreaMapper areaMapper;
    /**
     * 根据地区Id查找地区信息
     */
    public Area findAreaByAreaId(Integer areaId){
       return areaMapper.findAreaByAreaId(areaId);
    }
    /**
     * 根据id 查询区域信息
     */
    public Area selectByPrimaryKey(Integer id){
        return areaMapper.selectByPrimaryKey(id);
    }
    /**
     * 查找用户列表
     */
    public PageInfo<Area> getAreaList(String areaName, Integer currentPage, Integer limit){
        //利用PageHelper分页查询 注意：这个一定要放查询语句的前一行,否则无法进行分页,因为它对紧随其后第一个sql语句有效
        PageHelper.startPage(currentPage,limit);
        List<Area> areaList = areaMapper.getAreaList(areaName);
        PageInfo<Area> pageInfo = new PageInfo<Area>(areaList);
        return pageInfo;
    }
    /**
     * 添加
     */
    public void addArea(Area area){
        area.setCreateTime(new Timestamp(System.currentTimeMillis()));
        area.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        areaMapper.insert(area);
    }

    /**
     *修改
     */
    public void edit(Area area){
        area.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        areaMapper.updateByPrimaryKey(area);
    }

    /**
     * 删除
     *
     */
    public void delete(Integer id){
        areaMapper.deleteByPrimaryKey(id);
    }


    public List<Area> getAreaTree(String areaName) {
        List<Area> areaList = areaMapper.getAreaList(areaName);
//        List<Area> list = new ArrayList<Area>();
//        for(int i = 0; i<areaList.size(); i++){
//           String areaRank = list.add(areaList.get(i).getAreaRank());
//        }
        return areaList;
    }

}
