package com.gxuwz.mxb.lph.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.mxb.lph.dao.UserMapper;
import com.gxuwz.mxb.lph.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;

@Service
@Transactional
public class UserService {
    @Autowired
    private UserMapper userMapper;
    /**
     * 根据用户ID查找用户信息
     */
    public User findUserByUserId(String userId){
        return userMapper.findUserByUserId(userId);
    }

    /**
     * 根据ID查找用户信息
     */
    public User selectByPrimaryKey(Integer id){
        return userMapper.selectByPrimaryKey(id);
    }

    /**
     * 查找用户列表
     */
    public PageInfo<User> getUserList(String userName, Integer currentPage, Integer limit){
        //利用PageHelper分页查询 注意：这个一定要放查询语句的前一行,否则无法进行分页,因为它对紧随其后第一个sql语句有效
        PageHelper.startPage(currentPage,limit);
        List<User> userList = userMapper.getUserList(userName);
        PageInfo<User> pageInfo = new PageInfo<User>(userList);
        return pageInfo;
    }

    /**
     * 添加
     */
    public void addUser(User user){
        user.setCreateTime(new Timestamp(System.currentTimeMillis()));
        user.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        userMapper.insert(user);
    }

    /**
     *修改
     */
    public void edit(User user){
        user.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        userMapper.updateByPrimaryKey(user);
    }

    /**
     * 删除
     *
     */
    public void delete(Integer id){
        userMapper.deleteByPrimaryKey(id);
    }

}

